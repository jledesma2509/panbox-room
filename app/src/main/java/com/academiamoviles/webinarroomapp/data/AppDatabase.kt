package com.academiamoviles.webinarroomapp.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import java.util.concurrent.Executors

@Database(entities = [Mascota::class],version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase()  {

    abstract fun mascotaDao(): MascotaDao

    companion object{

        private var instancia : AppDatabase? = null
        private const val DATABASENAME = "BD_MASCOTAS"

        fun getInstance(context:Context) : AppDatabase{

            if(instancia == null){
                instancia = Room.databaseBuilder(
                    context,
                    AppDatabase::class.java,
                    DATABASENAME
                ).build()
            }
            return instancia as AppDatabase

        }


    }
}